import React, { Component } from 'react';
import {
  BackAndroid,
  AppRegistry,
  StyleSheet,
  Button,
  Text,
  View,
  PermissionsAndroid
} from 'react-native';

import Map from './Map';
import Scan from './Scan';

export default class GeoLocate extends Component {
  constructor() {
    super();

    this.state = {
      view: 'HOME'
    };

    this.createProvision = this.createProvision.bind(this);
    this.checkAndGrantPermissions = this.checkAndGrantPermissions.bind(this);
  }

  componentWillMount() {
    // Init provision
    // this.createProvision();
  }

  componentDidMount() {
    this.checkAndGrantPermissions();
  }

  createProvision() {
    fetch('http://164.61.67.172:4000/provision', {
      method: 'POST'
    }).then( res => {
      res.json().then( json => console.log(json) );
    });
  }

  async checkAndGrantPermissions() {
    const locationGranted = await PermissionsAndroid.requestPermission(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION, {
        'title': 'GeoLocate',
        'message': 'This application requires location services'
      }
    )

    const bluetoothGranted = await PermissionsAndroid.requestPermission(
      PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION, {
        'title': 'GeoLocate',
        'message': 'This application requires Bluetooth'
      }
    )
  }

  render() {
    const { view } = this.state;

    if (view == 'MAP')
      return <Map />;

    else if (view == 'SCAN')
      return <Scan />;

    return (
      <View style={styles.page}>
        <View style={styles.titleBar}>
          <Text style={styles.title}>GeoLocate</Text>
        </View>
        <View style={styles.buttonContainer}>
          <Button
            color='#8ACB88'
            onPress={() => this.setState({ view: 'MAP' })}
            title='Display Last Known Location'
            />
        </View>
        <View style={styles.buttonContainer}>
        <Button
          color='#8ACB88'
          onPress={() => this.setState({ view: 'SCAN' })}
          title='Scan for Module'
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  page: {
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 10,
    paddingBottom: 10
  },
  title: {
    color: '#8ACB88',
    fontWeight: 'bold',
    fontSize: 60
  },
  titleBar: {
    marginBottom: 15
  },
  buttonContainer: {
    height: 50,
    width: 320,
    marginBottom: 20
  }
});

AppRegistry.registerComponent('GeoLocate', () => GeoLocate);