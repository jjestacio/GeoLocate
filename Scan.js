import React, { Component } from 'react';

import {
	StyleSheet,
	Button,
	Text,
	View,
	ScrollView
} from 'react-native';

import { BleManager } from 'react-native-ble-plx'

// const PI_NAME = 'LG SH5B(F7)';
const PI_NAME = 'TARDIS';

const B = (props) => <Text style={{fontWeight: 'bold'}}>{props.children}</Text>;

export default class Scan extends Component {
	constructor() {
		super();

		this.state = {
			pi: null, // { uuid, name }
			gettingLoc: false,
			gettingLocErr: false,
			location: null,
			devices: []
		}

		this.foundDevices = {};
		this.manager = new BleManager();

		this.getLocation = this.getLocation.bind(this);
		this.reportLocation = this.reportLocation.bind(this);
	}

	componentWillMount() {
		// Can get pi information from server initially
		this.setState({ pi: { uuid: null, name: PI_NAME } });
	}

	componentDidMount() {
		this.manager.stopDeviceScan();
		this.manager.startDeviceScan(null, null, ( error, scannedDevice ) => {
			// if (error) {
			// 	this.manager.stopDeviceScan();
			// 	return
			// }

			const { uuid, name } = scannedDevice;
			const device = { uuid, name };

			// Check if device already found
			if (!this.foundDevices.hasOwnProperty(uuid)) {
				// Found the pi!
				if (device.name == this.state.pi.name) {
					console.log('Found the pi!');
					this.setState({ gettingLoc: true });
					this.getLocation();
				}

				this.foundDevices[uuid] = name;
				this.setState({ devices: [ ...this.state.devices, device ]});
			}
		});
	}

	componentWillUnmount() {Tardisthis.manager.stopDeviceScan();
		this.manager.destroy();
	}

	reportLocation() {
		fetch('http://164.67.61.172:4000/report', {
			method: 'POST',
			headers: new Headers({
				'Content-Type': 'application/json',
				Accept: 'application/json',
			}),
		    mode: 'cors',
		    cache: 'default',
			body: JSON.stringify({ 
				name: this.state.pi.name,
				location: this.state.location
			})
		}).then( res => {
			res.json().then( json => console.log(json) );
		});
	}

	getLocation() {
		const success = position => {
			this.setState({ gettingLoc: false });
			console.log('Position: ', position);

			const location = {
				latitude: position.coords.latitude,
				longitude: position.coords.longitude
			}

			this.setState({ location });
			this.reportLocation();
		}

		const error = err => {
			this.setState({ gettingLoc: false, gettingLocErr: true });
			console.log(err);
		}

		const options = {
			enableHighAccuracy: false,
			timeout: 12000,
			maximumAge: 0
		}

		navigator.geolocation.getCurrentPosition(success, error, options);
	}

	render() {
		const { devices, gettingLoc, gettingLocErr, location, pi } = this.state;

		// Grabbing Location
		if (gettingLoc) 
		return (
			<View style={styles.page}>
				<Text style={styles.found}>Found device!</Text>
				<Text style={styles.pi}>{ pi.name }</Text>
				<Text style={styles.found}>Grabbing location ... </Text>
			</View>
		)

		// Error grabbing location
		if (gettingLocErr)
		return (
			<View style={styles.page}>
				<Text style={styles.found}>Request to grab the location of this device timed out :(</Text>
			</View>
		);

		// Found device
		if (location) 
		return (
			<View style={styles.page}>
				<ScrollView>
				<Text style={styles.found}>Found device!</Text>
				<Text style={styles.pi}>{ pi.name }</Text>
				<Text style={styles.found}><B>Latitude</B>: { location.latitude }</Text>
				<Text style={styles.found}><B>Longitude</B>: { location.longitude }</Text>
				</ScrollView>
			</View>
		);

		return (
			<ScrollView style={styles.page}>
				{ devices.map( device => {
					return (
						<View style={styles.deviceContainer}>
							<Text style={styles.device}><B>uuid:</B> { device.uuid }</Text>
							<Text style={styles.device}><B>name:</B> { device.name }</Text>
						</View>
					);
				})}
			</ScrollView>
		);
	}
}

const styles = StyleSheet.create({
	page: {
		paddingLeft: 20,
		paddingRight: 20,
		paddingTop: 10,
		paddingBottom: 10
	},
	deviceContainer: {
		marginBottom: 10
	},
	device: {
		color: '#8ACB88',
		fontSize: 20
	},
	found: {
		color: '#8ACB88',
		fontSize: 28
	},
	error: {
		color: 'red',
		fontSize: 50
	},
	pi: {
		color: "#648381",
		fontSize: 40,
		fontWeight: 'bold',
		marginBottom: 10
	}
});
