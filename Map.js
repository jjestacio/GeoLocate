import React, { Component } from 'react';
import { WebView } from 'react-native';
import { MapView } from 'react-native-maps';

import {
  StyleSheet,
  Button,
  Text,
  View
} from 'react-native';

// const PI_NAME = 'LG SH5B(F7)';
const PI_NAME = 'TARDIS';

export default class Map extends Component {
	constructor() {
		super();

		this.state = {
			pi: null, // { uuid, name }
			location: null
		}

		this.getLastLocation = this.getLastLocation.bind(this);
	}

	componentWillMount() {
		// Can get pi information initially from server
		this.setState({ pi: { uuid: null, name: PI_NAME } });
		this.getLastLocation();
	}

	getLastLocation() {
		fetch('http://164.67.61.172:4000/find/' + PI_NAME, {
			method: 'GET'
		}).then( res => {
			res.json().then( json => {
				console.log(json);
				const locationParts = json.device.location.split(' ');
				this.setState({
					location: { latitude: locationParts[0], longitude: locationParts[1] }
				});
			});
		});
	}

	render() {
		const { location } = this.state;

		// if (location)
		// return (
		// 	<MapView
		// 		initialRegion={{ latitude: location.latitude, longitude: location.longitude }}
		// 	/>
		// );

		if (location)
		return (
			<WebView 
				source={{ uri: "https://www.google.com/maps/?q=" 
					+ location.latitude + "," + location.longitude }}
			/>
		);

		return (
			<View>
				<Text>No last known location!</Text>
			</View>
		);
	}
}